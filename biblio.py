from requests import Request, Session
from biblio_config import MATRICOLA, PASSWORD
from bs4 import BeautifulSoup

login_url = "http://static.unive.it/secure/zendAuth.php?dest=%2Fprenotazioni%2Faccesso%2Flogin"
pagina_elenco_prenotabili = "http://static.unive.it/prenotazioni/lemieprenotazioni"

parameter = {'j_username': MATRICOLA, 'j_password': PASSWORD}

s = Session()
req = Request('POST', login_url, params=parameter)
prepared = s.prepare_request(req)

resp = s.send(prepared)

if resp.status_code == 200:
    soup = BeautifulSoup(resp.text, 'html.parser')
    relaystate = soup.find_all('input')[0]['value']
    samlrequest = soup.find_all('input')[1]['value']

    if relaystate is not None and samlrequest is not None:

        parameter = {'RelayState': relaystate, 'SAMLRequest': samlrequest}
        req = Request('POST', "https://idp.unive.it/idp/profile/SAML2/POST/SSO", params=parameter)
        prepared = s.prepare_request(req)
        resp = s.send(prepared)

        print(resp.text)

        req = Request('POST', pagina_elenco_prenotabili)
        page = s.post(pagina_elenco_prenotabili)

        #print(page.text)

    else:
        print("Error")

s.close()
